{{/*
Create a default fully qualified app name for the postgres pgpool requirement.
*/}}
{{- define "keycloak.postgresql.pgpool" -}}
{{- $postgresContext := dict "Values" .Values.postgresql "Release" .Release "Chart" (dict "Name" "postgresql-ha") -}}
{{ include "postgresql-ha.pgpool" $postgresContext }}
{{- end }}

{{/*
Create a default fully qualified app name for the postgres postgresql requirement.
*/}}
{{- define "keycloak.postgresql.postgresql" -}}
{{- $postgresContext := dict "Values" .Values.postgresql "Release" .Release "Chart" (dict "Name" "postgresql-ha") -}}
{{ include "postgresql-ha.postgresql" $postgresContext }}
{{- end }}