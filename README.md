# Keycloak Umbrella Chart

[Keycloak](http://www.keycloak.org/) is an open source identity and access management for modern applications and services.

## Why another Keycloak Chart?

This is an umbrella chart to combine the [codecentric Keycloak Helm chart](https://github.com/codecentric/helm-charts/tree/master/charts/keycloak) with the [postgresql-ha Helm chart](https://github.com/bitnami/charts/tree/master/bitnami/postgresql) from Bitnami.
By default the Keycloak chart does use the non HA PostgreSQL version of the Bitnami chart and we won't modify that, this umbrella chart enables us to do that.

## Prerequisites Details

Surprise, we need a running Kubernetes environment as well as Helm 3+ installed.

## Configuration

The default password for the 'postgres' user and the replication user is 'changeMe!' for demonstration purposes how to configure the charts via values.yaml file.
For parameters please study the original chart documentation.

## Installing this Chart

```console
$ helm repo add helmcharts https://helmcharts.gitlab.io/stable
$ helm install keycloak helmcharts/keycloak
```

## Uninstalling the Chart

To uninstall the `keycloak` deployment:

```console
$ helm uninstall keycloak
```